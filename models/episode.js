'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Episode extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Episode.init({
    thumbnail: DataTypes.TEXT,
    publishDate: DataTypes.BIGINT,
    id: { 
      type: DataTypes.STRING,
      primaryKey: true
    },
    title: DataTypes.TEXT,
    image: DataTypes.TEXT,
    audioLengthSec: DataTypes.INTEGER,
    link: DataTypes.TEXT,
    description: DataTypes.TEXT,
    maybeAudioInvalid: DataTypes.BOOLEAN,
    audio: DataTypes.TEXT
  }, {
    sequelize,
    modelName: 'Episode',
  });
  return Episode;
};