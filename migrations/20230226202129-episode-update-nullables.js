'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.changeColumn('episodes', 'audio', {
      type: Sequelize.TEXT, 
      allowNull: true
    });
    await queryInterface.changeColumn('episodes', 'maybeAudioInvalid', {
      type: Sequelize.BOOLEAN, 
      allowNull: false, 
      defaultValue: false
    });
  },

  async down (queryInterface, Sequelize) {  
    await queryInterface.changeColumn('episodes', 'audio', {
      type: Sequelize.TEXT, 
      allowNull: false
    });
    await queryInterface.changeColumn('episodes', 'maybeAudioInvalid', {
      type: Sequelize.BOOLEAN, 
      allowNull: false
    });
  }
};
