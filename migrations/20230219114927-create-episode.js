'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('episodes', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      thumbnail: {
        type: Sequelize.TEXT
      },
      publishDate: {
        type: Sequelize.BIGINT,
        allowNull: false
      },
      id: {
        type: Sequelize.STRING, 
        allowNull: false, 
        unique: true
      },
      title: {
        type: Sequelize.TEXT,
        allowNull: false
      },
      image: {
        type: Sequelize.TEXT
      },
      audioLengthSec: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      link: {
        type: Sequelize.TEXT
      },
      description: {
        type: Sequelize.TEXT
      },
      maybeAudioInvalid: {
        type: Sequelize.BOOLEAN
      },
      audio: {
        type: Sequelize.TEXT, 
        allowNull: false
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('episodes');
  }
};