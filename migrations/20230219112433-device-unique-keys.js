'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.changeColumn('devices', 'deviceId', {
      type: Sequelize.DataTypes.STRING,
      allowNull: false,
      unique: true
    });
    await queryInterface.changeColumn('devices', 'notificationToken', {
      type: Sequelize.DataTypes.STRING,
      allowNull: false,
      unique: true
    });
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.changeColumn('devices', 'deviceId', {
      type: Sequelize.DataTypes.STRING,
      allowNull: true,
      unique: false
    });
    await queryInterface.changeColumn('devices', 'notificationToken', {
      type: Sequelize.DataTypes.STRING,
      allowNull: true,
      unique: false
    });
  }
};
