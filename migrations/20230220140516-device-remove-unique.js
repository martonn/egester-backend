'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.changeColumn('devices', 'userId', {
      type: Sequelize.DataTypes.STRING,
      unique: false
    });
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.changeColumn('devices', 'userId', {
      type: Sequelize.DataTypes.STRING,
      unique: false
    });
  }
};
