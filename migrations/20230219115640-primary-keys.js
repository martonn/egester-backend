'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.changeColumn('episodes', 'id', {
      type: Sequelize.DataTypes.STRING,
      primaryKey: true
    });
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.changeColumn('episodes', 'id', {
      type: Sequelize.DataTypes.STRING,
      primaryKey: false
    });
  }
};
