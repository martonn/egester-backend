'use strict';

const fs = require('fs');

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    const devices = JSON.parse(fs.readFileSync(`${__dirname}/resources/devices.json`).toString());
    await queryInterface.bulkInsert('devices', devices.map(device =>  {
      return {
        deviceId: device.deviceId,
        userId: device.userId,
        notificationToken: device.notificationToken,
        createdAt: new Date(),
        updatedAt: new Date()
      }
    }));
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('devices', null, {});
  }
};
