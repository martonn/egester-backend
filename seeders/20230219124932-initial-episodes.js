'use strict';

const fs = require('fs');

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    const episodes = JSON.parse(fs.readFileSync(`${__dirname}/resources/episodes.json`).toString());
    for (var offset = 0; offset < episodes.length; offset += 100) { 
      await queryInterface.bulkInsert('episodes', episodes.slice(offset, offset + 100).map(episode => {
        return {
          thumbnail: episode.thumbnail,
          publishDate: episode.pub_date_ms,
          id: episode.id,
          title: episode.title,
          image: episode.image,
          audioLengthSec: episode.audio_length_sec,
          link: episode.link,
          description: episode.description,
          maybeAudioInvalid: episode.maybe_audio_invalid,
          audio: episode.audio,
          createdAt: new Date(),
          updatedAt: new Date()
        }
      }));
    }
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('episodes', null, {});
  }
};
