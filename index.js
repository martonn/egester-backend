const axios = require('axios')
const express = require('express')
const app = express()
const http = require('http').Server(app)
const io = require('socket.io')(http)
const appleSignin = require('apple-signin-auth')
const { APNS, Notification, SilentNotification } = require('apns2')
const { Op } = require('sequelize')
const { Device, Episode } = require('./models')

require('dotenv').config()

const port = parseInt(process.env.PORT || 3000, 10)
const listenNotesPodcastID =  process.env.LISTEN_NOTES_PODCAST_ID
const listenNotesBaseURL = process.env.LISTEN_NOTES_BASE_URL
const listenNotesAPIKey = process.env.LISTEN_NOTES_API_KEY
const listenNotesWebhookSecret = process.env.LISTEN_NOTES_WEBHOOK_SECRET
const apiKey = process.env.API_KEY
const appleTeamId = process.env.APPLE_TEAM_ID
const apnsPrivateKeyId = process.env.APNS_PRIVATE_KEY_ID
const apnsPrivateKeyBase64 = process.env.APNS_PRIVATE_KEY_BASE64
const apnsDefaultTopic = process.env.APNS_DEFAULT_TOPIC
const apnsHost = process.env.APNS_HOST
const forcedNotificationSending = process.env.FORCED_NOTIFICATION_SENDING === 'true'

const socketEvent = {
  connection: 'connection', 
  disconnect: 'disconnect',
  currentEpisode: 'current_episode', 
  playbackState: 'playback_state',
  deviceListUpdate: 'device_list_update',
  activeDevice: 'active_device',
  playbackCommand: 'playback_command'
}

const logger = {
  info: (...args) => { console.log(`ℹ️  ${args.join(' - ')}`) }, 
  warning: (...args) => { console.log(`⚠️  ${args.join(' - ')}`) }, 
  error: (...args) => { console.log(`⛔️  ${args.join(' - ')}`) }
}

const mapEpisode = (episode) => {
  if (!episode) { return null }
  return {
    thumbnail: episode.thumbnail,
    pub_date_ms: episode.publishDate,
    id: episode.id,
    title: episode.title,
    image: episode.image,
    audio_length_sec: episode.audioLengthSec,
    link: episode.link,
    description: episode.description,
    maybe_audio_invalid: episode.maybeAudioInvalid === 1,
    audio: episode.audio 
  }
}

const apnsClient = new APNS({
  team: appleTeamId,
  keyId: apnsPrivateKeyId,
  signingKey: Buffer.from(apnsPrivateKeyBase64, 'base64'),
  defaultTopic: apnsDefaultTopic, 
  host: apnsHost
})

const fetchListenNotesEpisodes = async () => {
  const headers = { 'X-ListenAPI-Key': listenNotesAPIKey }
  const response = await axios.get(`${listenNotesBaseURL}/${listenNotesPodcastID}`, { headers: headers })
  return response.data.episodes
}

const getEpisodes = async (fromDate) => {
  let properties = {
    raw: true,
    order: [['publishDate', 'DESC']]
  }

  if (fromDate) {
    properties.where = {
      publishDate: {
        [Op.gt]: fromDate
      }
    }
  }

  const episodes = await Episode.findAll(properties)

  return episodes.map(episode => mapEpisode(episode))
}

const getEpisode = async (id) => {
  const episode = await Episode.findOne({ 
    raw: true, 
    where: {
      id: {
        [Op.eq]: id
      }
     }
   })
   return episode
}

const saveEpisodesIfNeeded = async (episodes) => {
  if (episodes.length === 0) {
    return
  }
  const objects = episodes.map(episode => {
    return {
      thumbnail: episode.thumbnail,
      publishDate: episode.pub_date_ms,
      id: episode.id,
      title: episode.title,
      image: episode.image,
      audioLengthSec: episode.audio_length_sec,
      link: episode.link,
      description: episode.description,
      maybeAudioInvalid: episode.maybe_audio_invalid,
      audio: episode.audio 
    }
  })
  await Episode.bulkCreate(objects)
}

const getDevices = async () => {
  return await Device.findAll({
    attributes: ['deviceId', 'userId', 'notificationToken'], 
    raw: true
  })
}

const getDevice = async (userId, deviceId) => {
  return await Device.findOne({ 
    where: {
      [Op.and]: [
        { userId: userId }, 
        { deviceId: deviceId }
      ]
    }
  })
}

const getDeviceListForUserID = (userID) => {
  const sockets = Array.from(io.sockets.sockets, ([key, value]) => value)
  return sockets.filter(socket => socket.userID === userID).map(socket => socket.device)
}

const getPreferredDeviceFromDeviceList= (deviceList) => {
  if (deviceList.length === 0) { 
    return null
  }
  return deviceList.sort((a, b) => a.connectionTime < b.connectionTime)[0]
}

const convertEpisode = (episode) => {
  return {
    thumbnail: episode.thumbnail, 
    pub_date_ms: episode.pub_date_ms, 
    id: episode.id,
    title: episode.title, 
    image: episode.image,
    audio_length_sec: episode.audio_length_sec,
    link: episode.link,
    description: episode.description,
    maybe_audio_invalid: episode.maybe_audio_invalid,
    audio: episode.audio
  }
}

const saveNotificationToken = async (userId, deviceId, notificationToken) => {
  try {
    const existingDevice = await getDevice(userId, deviceId)
    if (existingDevice) {
      existingDevice.set({ notificationToken: notificationToken, updatedAt: new Date() })
      await existingDevice.save()
    } else {
      await Device.create({
        userId: userId, 
        deviceId: deviceId, 
        notificationToken: notificationToken
      })
    }
  } catch (error) {
    return
  }
}

const deleteDevice = async (deviceId, userId) => {
  try {
    return await Device.destroy({ 
      where: {
        [Op.and]: [
          { userId: userId }, 
          { deviceId: deviceId }
        ]
      }
    })
  } catch (error) {
    return
  }
}

const sendNotificationForEpisode = async (episode) => {
  const devices = await getDevices()
  const notifications = devices.map(device => {
    return new Notification(device.notificationToken, {
      aps: {
        alert: {
          title: 'newEpisode',
          badge: 1,
          body: episode.title
        }, 
        'mutable-content': 1, 
        'imageURL': episode.image,
        'episodeID': episode.id
      }
    })
  })
  logger.info('Sending notifications to:', devices.map(device => device.notificationToken))
  await apnsClient.sendMany(notifications)
}

const sendNotification = async () => {
  const devices = await getDevices()
  const notifications = devices.map(device => new SilentNotification(device.notificationToken))
  logger.info('Sending notifications to:', devices.map(device => device.notificationToken))
  await apnsClient.sendMany(notifications)
}

const refetch = async (res) => {
  try {
    const localEpisodes = await getEpisodes()
    const remoteEpisodes = await fetchListenNotesEpisodes()

    const newEpisodes = remoteEpisodes.reduce((accumulator, remoteEpisode) => {
      let array = accumulator
      if (localEpisodes.filter(localEpisode => localEpisode.id == remoteEpisode.id).length == 0) {
        array.push(convertEpisode(remoteEpisode))
      }
      return array
    }, [])

    await saveEpisodesIfNeeded(newEpisodes)

    if (newEpisodes.length !== 0) {
      await sendNotificationForEpisode(newEpisodes[0])
    } else if (forcedNotificationSending && remoteEpisodes.length !== 0) {
      await sendNotificationForEpisode(remoteEpisodes[0])
    }

    logger.info('Refetch', 'Successful')
    res.status(200).send()
  } catch(error) {
    logger.error('Refetch', 'Error', error)
    res.status(500).send()
  }
}

app.use(express.json())
io.use(async (socket, next) => {
  if (!socket.handshake.headers.authorization) {
    return next(new Error('Unathorized'))
  }

  const token = socket.handshake.headers.authorization.replace('Bearer ', '')
  try {
    const verifyResult = await appleSignin.verifyIdToken(token, { ignoreExpiration: true })
    socket.join(verifyResult.sub)
    socket.userID = verifyResult.sub
    socket.device = {
      id: socket.handshake.headers['x-device-id'], 
      name: socket.handshake.headers['x-device-name'],
      type: socket.handshake.headers['x-device-type'],
      connectionTime: new Date().getTime() / 1000
    }
    logger.info('Token verification was successful', token)
    next()
  } catch(error) {
    logger.error('Token verification error', error)
    next(new Error('Unathorized'))
  }
})

app.get('/episodes', async (req, res) => {
  try {
    if (req.headers['x-api-key'] !== apiKey) {
      logger.warning('/episodes', 'GET', 'Bad API key')
      return res.status(401).send()
    }

    if (!req.query.from_date) {
      logger.info('/episodes', 'GET', 'Sent with no from_date')
      const episodes = await getEpisodes()
      return res.send(episodes)
    }
  
    const date = parseInt(req.query.from_date)
    if (isNaN(date)) {
      logger.warning('/episodes', 'GET', 'Invalid from_date')
      return res.status(400).send()
    }
    
    const filtered = await getEpisodes(date)
    if (filtered.length === 0) {
      logger.info('/episodes', 'GET', 'Empty episode list sent')
      return res.status(204).json([])
    }
    logger.info('/episodes', 'GET', 'Episode list sent with date', date)
    res.send(filtered)
  } catch(error) {
    logger.error('/episodes', 'GET', 'Error', error)
    res.status(500).send()
  }
})

app.get('/episodes/:id', async (req, res) => {
  try {
    if (req.headers['x-api-key'] !== apiKey) {
      logger.warning('/episodes/:id', 'GET', 'Bad API key')
      return res.status(401).send()
    }

    const episode = await getEpisode(req.params.id)
    if (!episode) {
      logger.warning('/episodes/:id', 'GET', 'Episode not found')
      return res.status(404).send()
    }

    res.redirect(episode.audio)
  } catch(error) {
    logger.error('/episodes/:id', 'GET', 'Error', error)
    res.status(500).send()
  }
})

app.get('/refetch', async (req, res) => {
  if (req.headers['x-api-key'] !== apiKey) {
    logger.warning('/refetch', 'GET', 'Bad API key')
    return res.status(401).send()
  }

  await refetch(res)
})

app.post('/refetch-webhook', async (req, res) => {
  if (req.headers['listenapi-webhook-secret'] !== listenNotesWebhookSecret) {
    logger.warning('/refetch-webhook', 'POST', 'Bad API key')
    return res.status(401).send()
  }
  if (req.body.podcast.id !== listenNotesPodcastID) {
    logger.warning('/refetch-webhook', 'POST', 'Bad ListenNotes podcast ID')
    return res.status(200).send()
  }

  await refetch(res)
})

app.post('/devices', async (req, res) => {
  if (req.headers['x-api-key'] !== apiKey) {
    logger.warning('/devices', 'POST', 'Bad API key')
    return res.status(401).send()
  }

  if (!req.headers.authorization) {
    logger.warning('/devices', 'POST', 'Bad auth token')
    return res.status(401).send()
  }

  if (!req.headers['x-device-id'] || !req.body['notificationToken']) {
    logger.warning('/devices', 'POST', 'Missing device id or notification token')
    return res.status(400).send()
  }

  const token = req.headers.authorization.replace('Bearer ', '')
  try {
    const verifyResult = await appleSignin.verifyIdToken(token, { ignoreExpiration: true })
    const userId = verifyResult.sub
    await saveNotificationToken(userId, req.headers['x-device-id'], req.body['notificationToken'])
    logger.info('/devices', 'POST', 'Device added (user id, device id)', userId, req.headers['x-device-id'])
    res.status(200).send()
  } catch(error) {
    logger.error('/devices', 'POST', 'Error', error)
    res.status(401).send()
  }
})

app.delete('/devices', async (req, res) => {
  if (req.headers['x-api-key'] !== apiKey) {
    logger.warning('/devices', 'DELETE', 'Bad API key')
    return res.status(401).send()
  }

  if (!req.headers.authorization) {
    logger.warning('/devices', 'DELETE', 'Bad auth token')
    return res.status(401).send()
  }

  if (!req.headers['x-device-id']) {
    logger.warning('/devices', 'DELETE', 'Missing device id')
    return res.status(400).send()
  }

  const token = req.headers.authorization.replace('Bearer ', '')
  try {
    const verifyResult = await appleSignin.verifyIdToken(token, { ignoreExpiration: true })
    const userId = verifyResult.sub
    deleteDevice(req.headers['x-device-id'], userId)
    logger.info('/devices', 'DELETE', 'Deleting device was successful (user id, device id)', userId, req.headers['x-device-id'])
    res.status(200).send()
  } catch(error) {
    logger.error('/devices', 'DELETE', 'Error', error)
    res.status(401).send()
  }
})

// TODO temp

// app.get('/devices', async (req, res) => {
//   try {
//     logger.info('/devices', 'GET', 'Device list sent')
//     res.status(200).json(await getDevices())
//   } catch (error) {
//     logger.error('/devices', 'GET', 'Error', error)
//     res.status(204).send() 
//   }
// })

// app.post('/notification/:token', async (req, res) => {
//   try {
//     logger.info('/notification/:token', 'POST', 'Sending notification to', req.params.token)

//     let notifications = []
//     if (req.query.is_silent === 'true') {
//       const notification = new SilentNotification(req.params.token)
//       notifications.push(notification)
//     } else {
//       const notification = new Notification(req.params.token, {
//         aps: {
//           alert: {
//             title: 'newEpisode',
//             badge: 1,
//             body: 'body'
//           }, 
//           'mutable-content': 1, 
//           'imageURL': 'https://production.listennotes.com/podcasts/totalcar-égéstér/minden-amit-a-uvdHyQERx3a-iHgxT60emAn.300x300.jpg',
//           'episodeID': 'dummyEpisodeId'
//         }
//       })
//       notifications.push(notification)
//     }

//     await apnsClient.sendMany(notifications)
//     res.status(200).send()
//   } catch (error) {
//     logger.error('/notification/:token', 'POST', 'Error', error)
//     res.status(500).send()
//   }
// })

io.on(socketEvent.connection, (socket) => {
  logger.info('Socket', socketEvent.connection, socket.userID, socket.device.id, socket.device.name)
  
  const deviceList = getDeviceListForUserID(socket.userID)
  io.to(socket.userID).emit(socketEvent.deviceListUpdate, deviceList)
  io.to(socket.userID).emit(socketEvent.activeDevice, getPreferredDeviceFromDeviceList(deviceList).id)

  socket.on(socketEvent.disconnect, () => {
    logger.info('Socket', socketEvent.disconnect, socket.userID, socket.device.id, socket.device.name)
    io.to(socket.userID).emit(socketEvent.deviceListUpdate, getDeviceListForUserID(socket.userID))
  })

  socket.on(socketEvent.currentEpisode, data => {
    logger.info('Socket', socketEvent.currentEpisode, data)
    socket.to(socket.userID).emit(socketEvent.currentEpisode, data)
  }) 

  socket.on(socketEvent.playbackState, data => {
    logger.info('Socket', socketEvent.playbackState, data)
    socket.to(socket.userID).emit(socketEvent.playbackState, data)
  })

  socket.on(socketEvent.activeDevice, data => {
    logger.info('Socket', socketEvent.activeDevice, data)
    io.to(socket.userID).emit(socketEvent.activeDevice, data)
  })

  socket.on(socketEvent.playbackCommand, data => {
    logger.info('Socket', socketEvent.playbackCommand, data)
    socket.to(socket.userID).emit(socketEvent.playbackCommand, data)
  })

})

http.listen(port, () => {
  logger.info(`Listening on port ${port}`)
})
